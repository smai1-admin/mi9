﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Mi9.Models;

namespace Mi9.Controllers
{
    public class ChallengeController : ApiController
    {
        // POST api/challenge
        public PayloadResult Post([FromBody]Program value)
        {
            Program model = value;

            if (model != null && ModelState.IsValid)
            {
                var result = model.payload.Where(p => p.drm && p.episodeCount > 0)
                            .Select(p => new Response
                            {
                                image = p.image.showImage,
                                slug = p.slug,
                                title = p.title
                            });
                return new PayloadResult() { response = result };
                
            }
            else
            {
                var customeError = new CustomeError() { error = "Could not decode request: JSON parsing failed" };
                var response = Request.CreateResponse(HttpStatusCode.BadRequest,customeError);
                throw new HttpResponseException(response);
            }
        }

    }
}
