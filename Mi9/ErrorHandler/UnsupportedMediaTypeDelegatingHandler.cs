﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using Mi9.Models;

namespace Mi9.ErrorHandler
{
    public class UnsupportedMediaTypeDelegatingHandler : DelegatingHandler
    {
        protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (request != null && request.Content != null && request.Content.Headers != null)
            {
                var contentType = request.Content.Headers.ContentType; 
                var formatters = request.GetConfiguration().Formatters;
                var hasFormatterForContentType = formatters.Any(formatter => formatter.SupportedMediaTypes.Contains(contentType));

                if (!hasFormatterForContentType)
                {
                    var message  = "{\"error\":\"Could not decode request: JSON parsing failed\"}";
                    return Task.Factory.StartNew(() => new HttpResponseMessage(HttpStatusCode.BadRequest)
                    {
                        Content = new StringContent(message)
                    });
                }
            }

            return base.SendAsync(request, cancellationToken);
        }
    }
}