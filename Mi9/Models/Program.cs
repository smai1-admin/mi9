﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Mi9.Models
{
    public class Program
    {
        [Required]
        public IEnumerable<Payload> payload;
        public uint skip;
        public uint take;
        public uint totalRecords;

    }
}