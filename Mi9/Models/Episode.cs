﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mi9.Models
{
    public class Episode
    {
        public string channel;
        public string channelLogo;
        public DateTime? date;
        public string html;
        public string url;
    }
}