﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Mi9.Models
{
    public class Payload
    {
        public string country;
        public string description;
        public bool drm;
        public uint episodeCount;
        public string genre;
        public Image image;
        public string language;
        public Episode nextEpisode;
        public string primaryColour;
        public IEnumerable<Season> seasons;
        public string slug;
        public string title;
        public string tvChannel;
    }
}